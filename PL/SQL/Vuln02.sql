select "ID", 
(case isadmin when 0 then '<font color=blue>' || htf.escape_sc(username) || '</font>' when 1 then '<font color=red>' || htf.escape_sc(username) || '</font>' else htf.escape_sc(username) end) "USERNAME",
"FULLNAME",
(case isadmin when 0 then '<font color=blue>User</font>' when 1 then '<font color=red>Admin</font>' else '<font color=red>status invalid for ' || username || '</font>' end) "Type"
from "#OWNER#"."USERS" 
  