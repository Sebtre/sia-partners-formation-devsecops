using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
//using System.Xml.Linq;
using RazorEngine;
using RazorEngine.Templating;

    namespace RazorVulnerableApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
           
            ViewBag.Template = @"Texte";
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(string razorTpl)
        {

            ViewBag.RenderedTemplate = Engine.Razor.RunCompile("@Model.razorTpl", "templateKey", null, new { razorTpl = razorTpl });
            ViewBag.Template = razorTpl;
            return View();
        }
    }
}
